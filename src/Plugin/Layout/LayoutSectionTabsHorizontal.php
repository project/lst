<?php

namespace Drupal\lst\Plugin\Layout;

/**
 * LST layout tabs horizontal.
 *
 * @Layout(
 *   id = "lst_layout_tabs_horizontal",
 *   label = @Translation("Tabs horizontal with optional top & bottom based on(VLSuite)"),
 *   path = "layouts/tabs-horizontal",
 *   template = "lst-layout-tabs-horizontal",
 *   library = "lst_layout_tabs/tabs-horizontal"
 * )
 */
class LayoutSectionTabsHorizontal extends LayoutSectionTabs {

  /**
   * {@inheritdoc}
   */
  protected function getPluginDefinitionIconMap() {
    $regions = [
      ['customize_tabs'],
    ];
    foreach ($this->configuration["options_tabs"]['options'] as $optionKey => $option) {
      $regions[] = [$optionKey];
    }
    return $regions;
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultWidth() {
    return '100';
  }

  /**
   * {@inheritdoc}
   */
  protected function getWidthOptions() {
    return [
      '100' => '100%',
      '80-auto' => '80%',
      '60-auto' => '60%',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function build(array $regions) {
    // Build the regions array with top, tabs, and bottom regions.
    $regions = parent::build($regions);
    return $regions;
  }

}
