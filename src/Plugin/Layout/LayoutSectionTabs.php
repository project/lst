<?php

namespace Drupal\lst\Plugin\Layout;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Layout\LayoutDefault;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Module lst Layout Section Tabs base.
 */
abstract class LayoutSectionTabs extends LayoutDefault implements PluginFormInterface, ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $tabs_base = new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
    $tabs_base->setPluginDefinitionRegions($configuration['tabs'] ?? []);
    $tabs_base->pluginDefinition->setIconMap($tabs_base->getPluginDefinitionIconMap());
    return $tabs_base;
  }

  /**
   * Get default width.
   *
   * @return string
   *   Default width.
   */
  abstract protected function getDefaultWidth();

  /**
   * Get plugin definition icon map.
   *
   * @return array
   *   Icon map.
   */
  abstract protected function getPluginDefinitionIconMap();

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $configuration = parent::defaultConfiguration();
    $configuration['column_widths'] = $this->getDefaultWidth();
    $configuration['identifier'] = NULL;
    $configuration['edge_to_edge_bg'] = TRUE;
    $configuration['optional_regions'] = TRUE;
    return $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $width_options = $this->getWidthOptions();
    $form['column_widths'] = [
      '#type' => count($width_options) > 1 ? 'select' : 'value',
      '#title' => $this->t('Main column widths'),
      '#default_value' => $this->configuration['column_widths'],
      '#options' => $this->getWidthOptions(),
      '#description' => $this->t('Choose the main column widths for this layout.'),
    ];
    $form['optional_regions'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show optional regions'),
      '#description' => $this->t('Show or not optional regions top & bottom. NOTE: Any related region content will not be deleted, just not shown when disabled.'),
      '#default_value' => $this->configuration['optional_regions'] ?? NULL,
    ];
    $form['edge_to_edge'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Edge to edge'),
      '#default_value' => $this->configuration['edge_to_edge'] ?? NULL,
    ];
    $form['edge_to_edge_bg'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Edge to edge background'),
      '#default_value' => $this->configuration['edge_to_edge_bg'] ?? NULL,
    ];
    $form['options_tabs'] = [
      '#type' => 'options_element',
      '#title' => $this->t('Section Tabs'),
      '#description' => $this->t('The tabs available in the layout section.'),
      '#multiple' => FALSE,
      '#optgroups' => TRUE,
      '#options' => $this->configuration['options_tabs']['options'] ?? '',
      '#default_value' => $this->configuration['options_tabs']['default_value'] ?? '',
      '#required' => TRUE,
      '#key_type_toggle' => $this->t('Custom keys'),
      '#key_type_toggled' => FALSE,
    ];
    $form['identifier'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Identifier'),
      '#description' => $this->t('Section identifier for anchor linking'),
      '#default_value' => $this->configuration['identifier'] ?? NULL,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    // @todo Implement validateConfigurationForm() method.
    // Check typed data.
    $options_custom_keys_tabs = [];
    foreach ($form_state->getValue('options_tabs')['options'] as $option_id => $option) {
      $option_id = is_numeric($option_id) ? strtolower($option) : strtolower($option_id);
      $option_id = str_replace(' ', '_', $option_id);
      $options_custom_keys_tabs[$option_id] = $option;
    }
    $form_state->getValue('options_tabs')['options'] = $options_custom_keys_tabs;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    // @todo Implement submitConfigurationForm() method.
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['options_tabs'] = $form_state->getValue('options_tabs');
    $this->configuration['optional_regions'] = $form_state->getValue('optional_regions');
    $this->configuration['column_widths'] = $form_state->getValue('column_widths');
    $this->configuration['edge_to_edge'] = $form_state->getValue('edge_to_edge');
    $this->configuration['edge_to_edge_bg'] = $form_state->getValue('edge_to_edge_bg');
    $this->configuration['identifier'] = $form_state->getValue('identifier');
  }

  /**
   * Get default tabs definition.
   *
   * @return array
   *   Default tabs definition.
   */
  protected function getDefaultTabs() {
    $regions = [];
    foreach ($this->configuration["options_tabs"]['options'] as $optionKey => $option) {
      $regions[$optionKey] = ['title' => $this->t($option)];
    }
    return $regions;
  }

  /**
   * Get tabs definition.
   *
   * @return array
   *   Tabs definition.
   */
  protected function getTabsDefinition() {
    return $this->configuration['tabs'] ?? $this->getDefaultTabs();
  }

  /**
   * Set plugin definition regions.
   *
   * @param array $tabs
   *   Tabs to set as regions.
   */
  protected function setPluginDefinitionRegions(array $tabs = []) {
    $regionMap = [];
    $regionMap['top'] = ['label' => $this->t('Top')];
    $tabs_loop = !empty($tabs) ? $tabs : $this->getTabsDefinition();
    foreach ($tabs_loop as $tab_index => $tab) {
      $regionMap[$tab_index] = ['label' => $tab['title']];
    }
    $regionMap['bottom'] = ['label' => $this->t('Bottom')];
    $this->pluginDefinition->setRegions($regionMap);
    $this->pluginDefinition->setDefaultRegion('tab_1');
  }

  /**
   * {@inheritdoc}
   */
  protected function getWidthOptions() {
    return [
      '100' => '100%',
      '80-auto' => '80%',
      '60-auto' => '60%',
    ];
  }

}
