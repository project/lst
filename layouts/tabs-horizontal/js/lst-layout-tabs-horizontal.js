(function($) {
  Drupal.behaviors.lstTabsHorizontal = {
    attach: function() {
      function updateTabs() {
        var windowWidth = $(window).width();
        if (windowWidth < 768) {
          $('.nav-tabs').addClass('accordion');
          $('.tab-pane a').on('click', function(e) {
            e.preventDefault();
            var $this = $(this),
              target = $this.data('target');
            $('.tab-pane').removeClass('show active');
            $this.closest('.tab-pane').addClass('show active');
          });
        } else {
          $('.nav-tabs').removeClass('accordion');
          $('.nav-tabs a').off('click');
          $('.nav-tabs a').on('click', function(e) {
            e.preventDefault();
            var $this = $(this),
              target = $this.data('target');
            $('.nav-tabs a').removeClass('active');
            $this.addClass('active');
            $('.tab-pane').removeClass('active show');
            $(target).addClass('active show');
          });
        }
      }
      // Ejecutar la función al cargar y redimensionar la ventana
      $(window).on('load resize', function() {
        updateTabs();
      });
    }
  };
})(jQuery)
